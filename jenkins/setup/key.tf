resource "aws_key_pair" "lch-keypair" {
  key_name   = "lch-keypair"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
  lifecycle {
    ignore_changes = [public_key]
  }
}

