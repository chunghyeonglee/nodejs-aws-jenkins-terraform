terraform {
  backend "s3" {
    bucket = "lchlch-node-aws-jenkins-terraform"
    key = "lchlch-node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}